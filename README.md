# Use Cases

- User uses Igotcha CMS to manage ad playlist
- Ad can be a media, a video, or a youtube link
- Ad can have multiple tags

Now company would like to add a feature to make playlist become smart playlist.

- Smart playlist can add(deploy) tags. If user deploy a tag to playlist, all ads has that tags name should appear in that playlist

**Example**:

We have following data:

```yaml
ad_1:
  name: Montreal Dorwin Falls
  url: http://igotchamedia.com/image.png
  tags:
    - waterfall
    - montreal
    - travel
ad_2:
  name: Quebec City
  url: http://igotchamedia.com/video.mp4
  tags:
    - quebec
    - travel
ad_3:
  name: NHL Hightlight
  url: https://www.youtube.com/watch?v=lwCXLtqjsy8
  tags:
    - sport

PlayList1:
  name: Travel
  deployed:
    - tags: [travel]
    - ads: [ad_3]
```

When user get list media play on PlayList1, he/she should receive `[ad_1, ad_2, ad_3]`

# Requirments:

- Design models to be able to deploy tags to playlist
- Write `deployed_medias` function to get a list of media on a playlist
- User would like to make the scheduler for a playlist, so that he can schedule for playing travel tag only from _2020-01-10_ to _2020-02-10_. Design and write `deployed_media_on(date)` function to get list of media play on a specific day.
